/**
 * 
 */
package org.prelle.rpgtool.xmpp;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.prelle.rpgtool.RPGToolDevice;

/**
 * @author prelle
 *
 */
public interface XMPPClientCallback {

	public void messageReceived(RPGToolDevice device, Chat chat, Message message);
	
}
