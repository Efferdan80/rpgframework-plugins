/**
 * 
 */
package org.prelle.rpgtool.xmpp;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.prelle.rpgtool.SessionScreen;
import org.prelle.rpgtool.sessionscreen.HandoutDisplay;
import org.prelle.rpgtool.sessionscreen.SessionScreenExtension;
import org.prelle.rpgtool.xmpp.ext.HandoutClientImpl;
import org.prelle.rpgtool.xmpp.ext.RPGToolPacket;
import org.prelle.rpgtool.xmpp.ext.ShowTextMessage;

/**
 * @author prelle
 *
 */
public class XMPPSessionScreenClient implements SessionScreen, MessageListener {
	
	private final static Logger logger = Logger.getLogger("rpgtool.xmpp");

	private String username;
	private XMPPConnection connection;
	private Chat chat;
	private HandoutClientImpl handout;
	private SessionScreenExtension[] allExtensions;
	
	//------------------------------------------------------------------
	/**
	 */
	XMPPSessionScreenClient(String username, XMPPConnection conn) {
		this.username = username;
		this.connection = conn;
		logger.debug("Setup chat to session screen at "+username);
		chat = connection.getChatManager().createChat(username, this);
		
		handout = new HandoutClientImpl(this);
		allExtensions = new SessionScreenExtension[]{
				handout
		};
	}

	//----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.rpgtool.SessionScreen#getIcon()
	 */
	public ImageIcon getIcon() {
		return null;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.SessionScreen#getRemotelySupportedExtensions()
	 */
	@Override
	public String[] getRemotelySupportedExtensions() {
		return new String[]{
				HandoutDisplay.NAME
		};
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.SessionScreen#getCommonExtensions()
	 */
	@Override
	public SessionScreenExtension[] getCommonExtensions() {
		return allExtensions;
	}

	//----------------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.prelle.rpgtool.SessionScreen#getCommonExtension(java.lang.String)
	 */
	@Override
	public SessionScreenExtension getCommonExtension(String extensionType) {
		for (SessionScreenExtension ext : getCommonExtensions()) {
			if (extensionType.equals(ext.getIdentifier()))
				return ext;
		}
		return null;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.RPGToolDevice#getUserName()
	 */
	@Override
	public String getUserName() {
		return username;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.RPGToolDevice#getDeviceType()
	 */
	@Override
	public Type getDeviceType() {
		return Type.SESSION_SCREEN;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.SessionScreen#showTextMessage(java.lang.String, int)
	 */
	@Override
	public void showTextMessage(String message, int seconds) {
		logger.debug("showTextMessage()");
		
		ShowTextMessage command = new ShowTextMessage(message, seconds);
		
		Message mess = new Message(username, Message.Type.normal);
		mess.addExtension(command);
		try {
			chat.sendMessage(mess);
		} catch (XMPPException e) {
			logger.error("Failed sending message.  "+e.getMessage(),e);
		}
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.MessageListener#processMessage(org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void processMessage(Chat chat, Message message) {
		// Only accept message from a session screen
		if (!message.getFrom().equals(username))
			return;
		logger.debug("Message received from session screen: "+message);
	}

	//------------------------------------------------------------------
	public void sendExtensionPacket(RPGToolPacket command) {
		logger.debug("sendExtensionPacket()");
		
		Message mess = new Message(username, Message.Type.normal);
		mess.addExtension(command);
		try {
			chat.sendMessage(mess);
		} catch (XMPPException e) {
			logger.error("Failed sending message.  "+e.getMessage(),e);
		}
	}

}
