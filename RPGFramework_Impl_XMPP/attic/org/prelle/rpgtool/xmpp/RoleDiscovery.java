package org.prelle.rpgtool.xmpp;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.prelle.rpgtool.Role;
import org.prelle.rpgtool.xmpp.ext.DiscoveryPacket;

/**
 * @author prelle
 *
 */
public class RoleDiscovery {

	private XMPPConnection conn;
	
	//------------------------------------------------------------------
	/**
	 */
	public RoleDiscovery(XMPPConnection conn) {
		this.conn = conn;
	}

	//------------------------------------------------------------------
	public void sendRequest(String to) {
		Message message = new Message(to, Message.Type.normal);
		message.addExtension(new DiscoveryPacket(Role.GAMEMASTER));
        conn.sendPacket(message);
		
	}

}
