/**
 * 
 */
package org.prelle.rpgtool.xmpp.ext;

import org.prelle.rpgtool.Role;

/**
 * @author prelle
 *
 */
public class DiscoveryPacket extends RPGToolPacket {

	private Role senderRole;

	//------------------------------------------------------------------
	/**
	 */
	public DiscoveryPacket(Role role) {
		super(MessageType.DISCOVERY);
		this.senderRole = role;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#getElementName()
	 */
	@Override
	public String getElementName() {
		return "discover";
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#toXML()
	 */
	@Override
	public String toXML() {
		 return "<rpgtool xmlns='"+RPGToolExtensionProvider.NAMESPACE+"'>" +
		         "<discover role='"+senderRole.name()+"'/>" +
		         "</rpgtool>";
	}

	//------------------------------------------------------------------
	/**
	 * @return the senderRole
	 */
	public Role getSenderRole() {
		return senderRole;
	}

}
