/**
 * 
 */
package org.prelle.rpgtool.xmpp.ext;

/**
 * @author prelle
 *
 */
public class ShowHandoutPacket extends RPGToolPacket {

	private String uri;

	//------------------------------------------------------------------
	/**
	 */
	public ShowHandoutPacket(String uri) {
		super(MessageType.SHOW_HANDOUT);
		this.uri = uri;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#getElementName()
	 */
	@Override
	public String getElementName() {
		return "handout";
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.packet.PacketExtension#toXML()
	 */
	@Override
	public String toXML() {
		 return "<rpgtool xmlns='"+RPGToolExtensionProvider.NAMESPACE+"'>" +
		         "<handout uri='"+uri+"'/>" +
		         "</rpgtool>";
	}

	//------------------------------------------------------------------
	public String getURI() {
		return uri;
	}

}
