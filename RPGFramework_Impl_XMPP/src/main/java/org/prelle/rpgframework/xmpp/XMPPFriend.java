/**
 * 
 */
package org.prelle.rpgframework.xmpp;

import java.net.URI;
import java.text.Collator;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smackx.packet.VCard;

import de.rpgframework.addressbook.DefaultIdentityInformation;
import de.rpgframework.addressbook.Identity;
import de.rpgframework.addressbook.IdentityInformation;
import de.rpgframework.addressbook.InfoType;
import de.rpgframework.addressbook.Parameter;
import de.rpgframework.addressbook.Scope;
import de.rpgframework.social.Friend;
import de.rpgframework.social.OnlineService;

/**
 * @author prelle
 *
 */
public class XMPPFriend implements Friend {
	
	private final static Logger logger = Logger.getLogger("rpgframework.xmpp");
	
	private XMPPClient network;
	private URI identifier;
	private RosterEntry roster;
	private VCard vcard;

	//-------------------------------------------------------------------
	/**
	 */
	public XMPPFriend(XMPPClient network, RosterEntry entry) {
		this.network = network;
		this.roster = entry;
		identifier = URI.create("xmpp:"+entry.getUser());
	}

	//-------------------------------------------------------------------
	public String toString() {
		return roster.getName()+" (xmpp:"+roster.getUser()+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		return roster.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#getFields()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<IdentityInformation> getFields() {
		String firstName = roster.getName();
		String lastName  = "";
		int pos = roster.getName().lastIndexOf(" ");
		if (pos>0) {
			firstName = roster.getName().substring(0, pos);
			lastName  = roster.getName().substring(pos+1, roster.getName().length());
		}
		if (vcard!=null) {
			if (vcard.getFirstName()!=null) 	firstName=vcard.getFirstName();
			if (vcard.getLastName()!=null) 	    lastName =vcard.getLastName();
		}		
		
		/*
		 * On the fly conversion
		 */
		List<IdentityInformation> ret = new ArrayList<IdentityInformation>();
		if (vcard!=null) {
			if (vcard.getNickName()!=null) 	    ret.add(new DefaultIdentityInformation(InfoType.NICKNAME, vcard.getNickName()));
			if (vcard.getMiddleName()!=null) 	ret.add(new DefaultIdentityInformation(InfoType.MIDDLENAME, vcard.getMiddleName()));
			if (vcard.getEmailHome()!=null) 
				ret.add(new DefaultIdentityInformation(InfoType.EMAIL, vcard.getEmailHome(), new AbstractMap.SimpleEntry<Parameter, String>(Parameter.SCOPE, Scope.PRIVATE.name())));
			if (vcard.getEmailWork()!=null) 
				ret.add(new DefaultIdentityInformation(InfoType.EMAIL, vcard.getEmailWork(), new AbstractMap.SimpleEntry<Parameter, String>(Parameter.SCOPE, Scope.BUSINESS.name())));
		} // vcard!=null
		
		// Ensure first and last name exists
		ret.add(new DefaultIdentityInformation(InfoType.FIRSTNAME, firstName));
		ret.add(new DefaultIdentityInformation(InfoType.LASTNAME, lastName));
		ret.add(new DefaultIdentityInformation(InfoType.IMPP_JABBER, roster.getUser()));
		
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#getAvatar()
	 */
	@Override
	public byte[] getAvatar() {
		if (vcard!=null)
			return vcard.getAvatar();
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Identity o) {
		return Collator.getInstance().compare(getDisplayName(), o.getDisplayName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.Friend#getOnlineService()
	 */
	@Override
	public OnlineService getOnlineService() {
		return network;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.Friend#getUser()
	 */
	@Override
	public String getUser() {
		return roster.getUser();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.Friend#getIdentifier()
	 */
	@Override
	public URI getIdentifier() {
		return identifier;
	}

	//-------------------------------------------------------------------
	public void setVCard(VCard vcard) {
		this.vcard = vcard;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#get(de.rpgframework.addressbook.InfoType)
	 */
	@Override
	public String get(InfoType type) {
		for (IdentityInformation field : getFields()) {
			if (field.getType()==type)
				return field.getValue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#get(de.rpgframework.addressbook.InfoType, de.rpgframework.addressbook.Scope)
	 */
	@Override
	public String get(InfoType type, Scope scope) {
		for (IdentityInformation field : getFields()) {
			if (field.getType()==type) {
				String scopeVal = field.getParameter(Parameter.SCOPE);
				if (scopeVal!=null && Scope.valueOf(scopeVal)==scope)
					return field.getValue();
			}
		}
		return null;
	}

}
