/**
 * 
 */
package org.prelle.rpgframework.xmpp;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.packet.VCard;
import org.prelle.rpgframework.api.InternalSocialNetworkCallback;

import de.rpgframework.addressbook.Contact;
import de.rpgframework.addressbook.InfoType;

/**
 * @author prelle
 *
 */
/**
 * @author prelle
 *
 */
public class RosterManager implements RosterListener {

	private final static Logger logger = Logger.getLogger("rpgtool.xmpp");
	
	private InternalSocialNetworkCallback callback;
	private XMPPConnection connection;
	private Roster roster;
	private Map<String, XMPPFriend> friendMap;
//	private Map<String, RPGToolDevice> knownDevices;
	
	//------------------------------------------------------------------
	/**
	 */
	public RosterManager(XMPPConnection conn, InternalSocialNetworkCallback callback) {
		this.connection = conn;
		this.callback   = callback;
		friendMap = new HashMap<String, XMPPFriend>();
//		knownDevices = new HashMap<String, RPGToolDevice>();
		
		roster = conn.getRoster();
		roster.addRosterListener(this);
//		scanRoster();
	}

	//------------------------------------------------------------------
	/**
	 * Overwrite the connection object with the new one
	 * 
	 * @param conn
	 */
	public void setConnection(XMPPConnection conn) {
		// Ignore, if identical
		if (conn==connection)
			return;
		// TODO: remove all announced component
		
		logger.info("Overwrite with new connection");
		roster = connection.getRoster();
//		scanRoster();
	}
	
	//------------------------------------------------------------------
	public Collection<String> getKnownUserIDs() {
		synchronized (friendMap) {
			return friendMap.keySet();
		}
	}

//	//------------------------------------------------------------------
//	public Collection<RPGToolDevice> getAvailableDevices() {
//		return knownDevices.values();
//	}


	//------------------------------------------------------------------
	void addFriend(XMPPFriend friend) {
		synchronized (friendMap) {
			friendMap.put(friend.getUser(), friend);
//			XMPPFriend friend = friendMap.get(entry.getUser());
//			if (friend==null) {
//				logger.debug("Update for a friend not in the roster: "+entry.getUser());
//				URI providerID = URI.create("xmpp:"+entry.getUser());
//				/*
//				 * Deduce first and last name from name
//				 */
//				String firstName = entry.getName();
//				String lastName  = "";
//				int pos = entry.getName().lastIndexOf(" ");
//				if (pos>0) {
//					firstName = entry.getName().substring(0, pos).trim();
//					lastName  = entry.getName().substring(pos+1, entry.getName().length()).trim();
//				}
//				logger.info("Firstname = "+firstName+"   lastname="+lastName);
//				
//				friend = new XMPPFriend(network, entry);
//				friendMap.put(entry.getUser(), friend);
//				return;
//			}
//
//			friend.set(InfoType.NICKNAME, entry.getName());

			callback.addedFriend(friend);
		}
	}

	//------------------------------------------------------------------
	public void updateFriend(String user, VCard vcard) {
		XMPPFriend friend = friendMap.get(user);
		if (friend==null) {
			logger.error("Got VCard for friend not in the roster: "+user);
			return;
		}
		
		if (vcard==null) {
			logger.error("Got empty VCard for "+user);
			return;
		}
		
		friend.setVCard(vcard);
		
		callback.updatedFriend(friend);
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.RosterListener#entriesAdded(java.util.Collection)
	 */
	@Override
	public void entriesAdded(Collection<String> usersAdded) {
		// TODO Auto-generated method stub
		logger.warn("TODO: entriesAdded("+usersAdded+")");
//		for (String user : usersAdded) {
//			logger.debug("Now "+user);
//			synchronized (friendMap) {
//				Contact friend = friendMap.get(user);
//				if (friend!=null)
//					continue;
//				try {
//					URI providerID = URI.create("xmpp:"+user);
//					friend = callback.getOrCreateFriend(providerID, user, user, user);
//					friendMap.put(user, friend);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.RosterListener#entriesDeleted(java.util.Collection)
	 */
	@Override
	public void entriesDeleted(Collection<String> arg0) {
		// TODO Auto-generated method stub

	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.RosterListener#entriesUpdated(java.util.Collection)
	 */
	@Override
	public void entriesUpdated(Collection<String> arg0) {
		// TODO Auto-generated method stub

	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.RosterListener#presenceChanged(org.jivesoftware.smack.packet.Presence)
	 */
	@Override
	public void presenceChanged(Presence presence) {
		logger.trace("presenceChanged: "+presence+" of "+presence.getFrom());
		// Find the roster entry for the user. To get it, remove resource name
		String user = XMPPClient.getWithoutResource(presence.getFrom());
		RosterEntry entry = roster.getEntry(user);
		if (entry==null) {
			logger.warn("Strange - got a presence message for someone not in my roster: "+presence.getFrom());
			return;
		}
		
//		logger.debug("Presence of "+entry.getName()+" changed to "+presence.getType()+"/"+presence.getStatus());
		
		switch (presence.getType()) {
		case available:
			// Only process presence messages that belong to the RPGTool
//			PacketExtension ext = presence.getExtension(RPGToolExtensionProvider.NAMESPACE);
//			if (ext==null) 
//				return;		
//			DiscoveryPacket discovery = (DiscoveryPacket)ext;
			
			handleAvailable(presence, entry); //, discovery);
			break;
		case unavailable:
			handleUnavailable(presence, entry);
			break;
		}
	}

	//------------------------------------------------------------------
	private void handleAvailable(Presence presence, RosterEntry entry) {//, DiscoveryPacket discovery) {
		logger.trace("Found a RPGTool device for "+entry.getName()+": ");
//		switch (discovery.getSenderRole()) {
//		case SESSION_SCREEN:
//			logger.info("Session screen appeared ("+entry.getName()+" under "+presence.getFrom()+")");
//			XMPPSessionScreenClient screen = new XMPPSessionScreenClient(presence.getFrom(), connection);
//			knownDevices.put(presence.getFrom(), screen);
//			RPGToolDeviceRegistry.announceDevice(screen);
////			for (RPGToolComponentListener list : listener) {
////				list.sessionScreenAppeared(screen);
////			}
//			break;
//		case GAMEMASTER:
//			logger.info("Gamemaster appeared ("+entry.getName()+" under "+presence.getFrom()+")");
//			XMPPGamemasterClient gm = new XMPPGamemasterClient(presence.getFrom(), connection);
//			knownDevices.put(presence.getFrom(), gm);
////			for (RPGToolComponentListener list : listener) {
////				list.sessionScreenAppeared(screen);
////			}
//			break;
//		default:
//			logger.warn("No action for new "+discovery.getSenderRole());
//		}

	}

	//------------------------------------------------------------------
	private void handleUnavailable(Presence presence, RosterEntry entry) {
//		RPGToolDevice device = knownDevices.get(presence.getFrom());
//		if (device==null) {
//			PacketExtension ext = presence.getExtension(RPGToolExtensionProvider.NAMESPACE);
//			if (ext!=null) { 
//				DiscoveryPacket discovery = (DiscoveryPacket)ext;
//				logger.info("An unknown "+discovery.getSenderRole()+" disappeared");
//			}
//			return;
//		}
//		
//		switch (device.getDeviceType()) {
//		case SESSION_SCREEN:
//			logger.info("Session screen disappeared ("+entry.getName()+" under "+presence.getFrom()+")");
//			RPGToolDeviceRegistry.removeDevice(device);
////			for (RPGToolComponentListener list : listener) {
////				list.sessionScreenAppeared((SessionScreen) device);
////			}
//			break;
//		default:
//			logger.warn("No action for disappeared "+device.getDeviceType());
//		}

	}

	//------------------------------------------------------------------
	/**
	 * Get the component that is registered for that user+resource.
	 * 
	 * @param participant
	 * @return Component or NULL, if there is none
	 */
//	public RPGToolDevice getDeviceFor(String user) {
//		return knownDevices.get(user);
//	}

}
