/**
 * 
 */
package org.prelle.rpgframework.xmpp;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

/**
 * @author prelle
 *
 */
public class ChatHandler implements ChatManagerListener, MessageListener {

	private final static Logger logger = Logger.getLogger("rpgtool.xmpp");

	private RosterManager rosterManager;
//	private XMPPClientCallback callback;
	
	//------------------------------------------------------------------
	/**
	 */
	public ChatHandler(RosterManager rostMgr) { //, XMPPClientCallback callback) {
		if (rostMgr==null)
			throw new NullPointerException("Null roster");
//		if (callback==null)
//			throw new NullPointerException("Null callback");
		this.rosterManager = rostMgr;
//		this.callback = callback;
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.ChatManagerListener#chatCreated(org.jivesoftware.smack.Chat, boolean)
	 */
	@Override
	public void chatCreated(Chat chat, boolean createdLocally) {
		logger.debug("Chat created: "+chat.getParticipant()+"  locally="+createdLocally);
		chat.addMessageListener(this);

	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.MessageListener#processMessage(org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void processMessage(Chat chat, Message mess) {
		String from = mess.getFrom();
		logger.info("RCV from "+from+" | "+chat.getParticipant()+": "+mess.getBody());
		logger.debug("check in roster "+rosterManager);
		
//		RPGToolDevice device = rosterManager.getDeviceFor(from);
//		logger.debug("returned with "+device);
//		if (device==null) {
//			logger.info("Message from an entity that isn't a component: "+mess.getBody());
//			return;
//		}
//		logger.debug("Inform "+callback.getClass());
//		logger.info(device.getDeviceType()+" sends a message");
//		
//		callback.messageReceived(device, chat, mess);
	}

}
