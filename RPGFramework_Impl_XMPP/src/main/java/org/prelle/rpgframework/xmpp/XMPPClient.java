/**
 * 
 */
package org.prelle.rpgframework.xmpp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.provider.DelayInfoProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.prelle.rpgframework.api.InternalSocialNetwork;
import org.prelle.rpgframework.api.InternalSocialNetworkCallback;
import org.prelle.rpgframework.xmpp.ext.RPGToolExtensionProvider;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

import de.rpgframework.ConfigChangeListener;
import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author prelle
 *
 */
/**
 * @author prelle
 *
 */
public class XMPPClient implements Runnable, InternalSocialNetwork, 
	MessageListener, ConfigChangeListener {

	private final static Logger logger = Logger.getLogger("rpgframework.xmpp");
	private final static String RESOURCE_GAMEMASTER = "Gamemaster";
	private final static String RESOURCE_SESSIONSCREEN = "SessionScreen";

	private final static String PROP_USER = "user";
	private final static String PROP_PASS = "pass";
	private final static String PROP_HOST = "host";
	private final static String PROP_PORT = "port";
	
	private static Preferences PREF = Preferences.userRoot().node("/org/prelle/rpgframework/online/social/xmpp");
	
	enum ConnectionState { TRY_CONNECTION, CONNECTED, READY, DISCONNECT_REQUESTED, TRY_RECONNECT}

	private ConfigContainer configRoot;
	private ConfigOption user;
	private ConfigOption pass;
	private ConfigOption server;
	private ConfigOption port;

	private InternalSocialNetworkCallback callback;

	private ConnectionState state;
	private XMPPConnection conn;
//	private Role role;
//	private ProtocolState myState = ProtocolState.UNCONFIGURED;

	private Thread connectorThread;
	private List<String> serverList;
	private String domainName;
//	private String userName;
//	private String myResourceName;
	private String self;
	
//	private List<RPGToolComponentListener> componentListener;
//	private XMPPClientCallback callback;
	private RosterManager gmRoster;
	private ChatHandler chatHandler;

	//------------------------------------------------------------------
	/**
	 */
	public XMPPClient() {
//		this.role = Role.GAMEMASTER;
		
		state = ConnectionState.TRY_RECONNECT;
		serverList = new ArrayList<String>();
//		callback = new DummyCallback();


		ProviderManager iqManager = ProviderManager.getInstance();
		iqManager.addIQProvider("vCard", "vcard-temp", new VCardProvider());
//		iqManager.addExtensionProvider("c", "http://jabber.org/protocol/caps", new CapsExtensionProvider());
		iqManager.addExtensionProvider("delay", "urn:xmpp:delay", new DelayInfoProvider());
		iqManager.addExtensionProvider("rpgtool", RPGToolExtensionProvider.NAMESPACE, new RPGToolExtensionProvider());
	}
//	
//	//------------------------------------------------------------------
//	private void prepareConfigNode() {
//		configRoot = new ConfigContainerImpl(PREF, "xmpp");
//		user = new ConfigOptionImpl(PROP_USER, ConfigOption.Type.TEXT, null);
//		pass = new ConfigOptionImpl(PROP_PASS, ConfigOption.Type.PASSWORD, null);
//		server = new ConfigOptionImpl(PROP_HOST, ConfigOption.Type.TEXT, null);
//		port = new ConfigOptionImpl(PROP_PORT, ConfigOption.Type.NUMBER, 5222);
//		
//		configRoot.addChild(user);
//		configRoot.addChild(pass);
//		configRoot.addChild(server);
//		configRoot.addChild(port);
//		configRoot.addListener(this);
//		
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#getConfigurationOptions()
	 */
	@Override
	public ConfigContainer getConfigurationOptions() {
		return configRoot;
	}

//	//------------------------------------------------------------------
//	/**
//	 */
//	public XMPPClient(Role role, XMPPClientCallback callback) {
//		this.role = role;
//		this.callback = callback;
////		this.componentListener = new ArrayList<RPGToolComponentListener>();
//		
//		state = ConnectionState.TRY_RECONNECT;
//		serverList = new ArrayList<String>();
//
//		ProviderManager.getInstance().addExtensionProvider("rpgtool", RPGToolExtensionProvider.NAMESPACE, new RPGToolExtensionProvider());
//	}

	//------------------------------------------------------------------
	private void evaluateConfig() {
		String identity = user.getStringValue();
		String host     = server.getStringValue();
		
		if (identity!=null) {
		// Get domain name from identity
		int index = identity.indexOf('@');
		if (index>0) {
//			userName   = config.getIdentity().substring(0, index);
			domainName = identity.substring(index+1);
		} else {
//			userName   = config.getIdentity();
			domainName = host;
			logger.warn("No domain part in URI "+identity);
		}
		}
		resetServerList();
	}

	//------------------------------------------------------------------
	private void start() {
		logger.debug("start");
		
		connectorThread = new Thread(this, "XMPPConnector");
		connectorThread.start();
	}

//	//------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgtool.InterComponentProtocol#startAs(org.prelle.rpgtool.RPGToolDevice)
//	 */
//	@Override
//	public void startAs(RPGToolDevice realDevice) {
//		switch (realDevice.getDeviceType()) {
//		case GAMEMASTER:
//			role = Role.GAMEMASTER;
//			start();
//			break;
//		case SESSION_SCREEN:
//			role = Role.SESSION_SCREEN;
//			setCallback(new XMPPSessionScreenServer(this, (SessionScreen)realDevice ));
//			start();
//			break;
//		default:
//			logger.warn("Don't know how to start as "+realDevice.getDeviceType());
//		}
//	}

	//------------------------------------------------------------------
	private void resetServerList() {
		serverList.clear();
		if (server.getValue()!=null)
			serverList.add(server.getStringValue());
	}

	//------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		while (true) {
			try {
				switch (state) {
				case TRY_RECONNECT:
					// Ensure there is a configuration
					if (user.getStringValue()==null) {
						logger.debug("  check  "+user+"  "+user.getPathID());
						logger.debug("No XMPP configuration yet");
						Thread.sleep(10000);  // Wait 10 seconds
						continue;
					}
					evaluateConfig();
					
					if (serverList.isEmpty()) {
						if (buildServerList()) {
							state = ConnectionState.TRY_CONNECTION;
							continue;
						}
						state = ConnectionState.TRY_RECONNECT;
					} else {
						state = ConnectionState.TRY_CONNECTION;
						continue;						
					}
					break;
				case TRY_CONNECTION:
					if (serverList.isEmpty()) {
						
					} else {
						String server = serverList.get(0);
						if (makeConnection(server))
							continue;
					}
					break;
				case DISCONNECT_REQUESTED:
					logger.debug("Disconnect requested on "+conn);
					if (conn!=null)
						conn.disconnect();
					conn = null;
					resetServerList();
					Thread.sleep(1000);
					state = ConnectionState.TRY_RECONNECT;
					break;
				case CONNECTED:
					if (gmRoster==null) {
						gmRoster = new RosterManager(conn, callback);
						// Add memorized component listener
//						for (RPGToolComponentListener list : componentListener)
//							gmRoster.addComponentListener(list);
//						componentListener.clear();
					} else 
						gmRoster.setConnection(conn);
					conn.getRoster().addRosterListener(gmRoster);
					readRoster(conn);
					// Set chat handler
					chatHandler = new ChatHandler(gmRoster); // , callback);
					conn.getChatManager().addChatListener(chatHandler);
					sendPresence();
//					discoverServerInformation(domainName);
//					doDiscovery();
					state = ConnectionState.READY;
					BabylonEventBus.fireEvent(BabylonEventType.SOCIAL_NETWORK_STATE_CHANGED, ConnectionState.READY, this);
					readVCards(conn);
					continue;
				case READY:
					// Do nothing
//					logger.debug("do nothing");
					break;
				default:
					logger.warn("Don't know what to do in state: "+state);
				} // switch

				// Wait a moment
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				logger.debug("Interrupted - in state "+state);
			} catch (Exception e) {
				logger.error(e.toString(),e);
				state = ConnectionState.TRY_RECONNECT;
			}
		}  // while true
	}

	//------------------------------------------------------------------
	/**
	 * @return TRUE if operation was successful
	 */
	private boolean buildServerList() {
		logger.debug("----------------------------------buildServerList()");
		// Get domain name from identity
		int index = user.getStringValue().indexOf('@');
		if (index<=0) {
			logger.warn("No domain part in URI "+user.getStringValue());
			return false;
		}
		String service = "_xmpp-client._tcp."+domainName;
		logger.debug("Find Jabber server for "+domainName+" / "+service);
		try {
			Record [] records = new Lookup(service, Type.SRV).run();
			if (records==null) {
				logger.warn("DNS failed for "+service);
				return false;
			}
			// Sort arrays
			Arrays.sort(records, new Comparator<Record>() {
				@Override
				public int compare(Record rec1, Record rec2) {
					return (Integer.valueOf(((SRVRecord)rec1).getPriority()).compareTo( ((SRVRecord)rec2).getPriority()));
				}
			});
			// Store them
			for (int i = 0; i < records.length; i++) {
				SRVRecord srv = (SRVRecord) records[i];
				logger.debug("Host " + srv.getTarget() + " has preference "+ srv.getPriority()+" / "+srv.getTarget().labels());
				String hostName = srv.getTarget().toString();
				hostName = hostName.substring(0, hostName.length()-1);
				serverList.add(hostName);
			}
		} catch (TextParseException e) {
			logger.error(e.toString(),e);
		}
		return true;
	}

	//------------------------------------------------------------------
	public static String getWithoutResource(String jabberID) {
		int idx = jabberID.indexOf('/');
		if (idx<0)
			return jabberID;
		return jabberID.substring(0, idx);
	}

	//------------------------------------------------------------------
	public static String getResource(String jabberID) {
		int idx = jabberID.indexOf('/');
		if (idx<0)
			return "";
		return jabberID.substring(idx+1);
	}

	//------------------------------------------------------------------
	/**
	 */
	private boolean makeConnection(String server) {
		logger.info("Try to connect as "+user.getStringValue()+" to "+server+"  "+pass.getStringValue());
		
		buildServerList();

	    logger.info("SASL = "+SASLAuthentication.getRegisterSASLMechanisms());
		
		Connection.DEBUG_ENABLED = false;
		ConnectionConfiguration connConfig = new ConnectionConfiguration(server, 5222);
		// pass some connection options
		connConfig.setSASLAuthenticationEnabled(true);

//		SASLAuthentication.supportSASLMechanism("GSSAPI", 1);
		SASLAuthentication.supportSASLMechanism("PLAIN", 1);
//		SASLAuthentication.supportSASLMechanism("DigestMD5", 1);
		connConfig.setReconnectionAllowed(false);
		connConfig.setRosterLoadedAtLogin(true);
		connConfig.setDebuggerEnabled(false);
		conn = new XMPPConnection(connConfig);
		if (gmRoster==null) {
			gmRoster = new RosterManager(conn, callback);
			// Add memorized component listener
//			for (RPGToolComponentListener list : componentListener)
//				gmRoster.addComponentListener(list);
//			componentListener.clear();
		} else 
			gmRoster.setConnection(conn);
		logger.info("+++++++Add roster listener++++++++++++++++++++");
		conn.getRoster().addRosterListener(gmRoster);

		try {
			conn.connect();
			conn.login(
					user.getStringValue(), 
					pass.getStringValue(), 
					"rpgframework"
					);
		} catch (XMPPException e) {
			logger.warn("Login failed! "+e.getMessage());
			// Ugly detection
			if (e.getMessage().equals("SASL authentication failed")) {
				logger.warn("Perhaps the password is missing?");
				return false;
			}
			if (e.getMessage().equals("SASL authentication PLAIN failed: not-authorized")) {
				logger.warn("Invalid username or password");
				return false;
			}
			logger.error("XMPP-Error: "+e.getXMPPError());
			logger.error("Stream-Error: "+e.getStreamError());
			logger.debug("Full exception: "+e,e);
			return false;
		}
		logger.info("Successfully connected to XMPP server");

		state = ConnectionState.CONNECTED;
		
        // Register that a new feature is supported by this XMPP entity
        ServiceDiscoveryManager discoManager = ServiceDiscoveryManager.getInstanceFor(conn);        
        discoManager.addFeature(RPGToolExtensionProvider.NAMESPACE);

		return true;
	}
	
	//------------------------------------------------------------------
	private void discoverUserInformation(String jid) {
		 // Obtain the ServiceDiscoveryManager associated with my Connection
	      ServiceDiscoveryManager discoManager = ServiceDiscoveryManager.getInstanceFor(conn);
	      
	      try {
			// Get the information of a given XMPP entity
			  // This example gets the information of a conference room
			  DiscoverInfo discoInfo = discoManager.discoverInfo(jid);

			  // Get the discovered identities of the remote XMPP entity
			  Iterator<DiscoverInfo.Identity> it = discoInfo.getIdentities();
			  // Display the identities of the remote XMPP entity
			  while (it.hasNext()) {
			      DiscoverInfo.Identity identity = (DiscoverInfo.Identity) it.next();
			      logger.debug("  Info Name    ="+identity.getName());
			      logger.debug("  Info Type    ="+identity.getType());
			      logger.debug("  Info Category="+identity.getCategory());
			  }

			  // Get the discovered identities of the remote XMPP entity
			  Iterator<DiscoverInfo.Feature> it2 = discoInfo.getFeatures();
			  // Display the identities of the remote XMPP entity
			  while (it2.hasNext()) {
			      DiscoverInfo.Feature feature = (DiscoverInfo.Feature) it2.next();
			      logger.debug("  Feature Var ="+feature.getVar()+"   // "+feature);
			  }

			  // Check if room is password protected
			  discoInfo.containsFeature("muc_passwordprotected");
		} catch (Exception e) {
			logger.error(e.toString(),e);
		}
	}

	//------------------------------------------------------------------
	private void readRoster(XMPPConnection conn) {
		logger.debug("Read roster");
		
		for (RosterGroup group : conn.getRoster().getGroups()) {
			logger.debug("Group "+group.getName());
		}
		
		for (RosterEntry entry : conn.getRoster().getEntries()) {
			XMPPFriend friend = new XMPPFriend(this, entry);
			gmRoster.addFriend(friend);
			logger.debug("New friend: "+friend);
			callback.addedFriend(friend);
//			discoverUserInformation(entry.getUser());
		}
//		System.exit(0);
		logger.debug("Read roster finished");
	}

	//------------------------------------------------------------------
	private void readVCards(XMPPConnection conn) {
		logger.debug("Read VCards");

		for (String user : gmRoster.getKnownUserIDs()) {
			VCard vcard = new VCard();
			try {
				vcard.load(conn, user);
				gmRoster.updateFriend(user, vcard);
			} catch (XMPPException e) {
				logger.warn("Failed loading VCard for "+user+": "+e);
			}
		}
		logger.debug("Read VCards finished");
	}

	//------------------------------------------------------------------
	/**
	 * Set presence state
	 */
	private void sendPresence() {
		logger.debug("Send presence");
		
		Presence presence = new Presence(Presence.Type.available);
		presence.setMode(Mode.dnd);
		presence.setStatus("In einer Rollenspielrunde");
//		presence.addExtension(new DiscoveryPacket(role));
		conn.sendPacket(presence);
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.MessageListener#processMessage(org.jivesoftware.smack.Chat, org.jivesoftware.smack.packet.Message)
	 */
	@Override
	public void processMessage(Chat chat, Message message) {
		logger.debug("processMessage: "+message.getFrom()+" -> "+message.getTo()+"  in chat with "+chat.getParticipant()+" : "+message.getBody());
		try {
			// Filter out echo
			if (message.getFrom().equals(self)) {
				// Is echo
				return;
			}
			
			String text    = message.getBody();
//			String jid     = getWithoutResource(message.getFrom());
			String appName = getResource(message.getFrom());
			logger.info("Process message "+message.getBody()+" from "+appName);
			
//			// Try to obtain username from roser
//			RosterEntry entry = roster.getEntry(jid);
//			String username = jid;
//			if (entry!=null)
//				entry.getName();
//			
//			switch (role) {
//			case GAMEMASTER:
//				// Expect SessionScreen
//				if (appName.equals(RESOURCE_SESSIONSCREEN)) {
//					if (text.equals("Pong")) {
//						checkSSClientAlivenessCount = 0;
//						// Announce
//						if (ssClient==null) {
//							logger.info("Found session screen");
//							ssClient = new XMPPSessionScreenClient(username, chat);
//							for (RPGToolComponentListener list : componentListener)
//								list.sessionScreenAppeared(ssClient);
//							
//							checkSSClientAliveness = new TimerTask() {								
//								@Override
//								public void run() {
//									if (checkSSClientAlivenessCount==3) {
//										// 5 Seconds without response - if we had a session screen remove it
//										if (ssClient!=null) {
//											logger.info("Session screen disappeared");
//											ssClient = null;
//											checkSSClientAliveness.cancel(); // No further tests
//											for (RPGToolComponentListener list : componentListener)
//												list.sessionScreenDisappeared(ssClient);
//										}
//									}
//									
//								}
//							};
//							timer.scheduleAtFixedRate(checkSSClientAliveness, 1000, 4000);
//							return;
//						}
//						// Simple re-announce
//						// TODO Alive-check
//						return;
//					}
//					logger.warn("Unprocessed: "+text);
//				} else {
//					logger.debug("Message from unknown resource");
//					try {
//						chat.sendMessage("I am a RPGTool - you cannot chat with me.");
//					} catch (XMPPException e) {
//						logger.warn(e);
//					}
//				}
//				break;
//			case SESSION_SCREEN:
//				// Expect Gamemaster
//				if (appName.equals(RESOURCE_GAMEMASTER)) {
//					if (text.equals("Ping")) {
//						logger.debug("Respond gamemaster alive request");
//						try {
//							chat.sendMessage("Pong");
//						} catch (XMPPException e) {
//							logger.warn(e);
//						}
//						return;
//					} else
//						logger.debug("Syntax error: ["+text+"]");
//				} else {
//					logger.debug("Message from unknown resource");
//					try {
//						chat.sendMessage("I am just a screen - you cannot chat with me.");
//					} catch (XMPPException e) {
//						logger.warn(e);
//					}
//				}
//				break;
//			default:
//				logger.error("Unknown role");
//			}
			
			logger.warn("Unprocessed: ["+text+"]");
		} catch (Exception e) {
			logger.error(e.toString(),e);
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.api.InternalSocialNetwork#initialize(org.prelle.rpgframework.api.InternalSocialNetworkCallback)
	 */
	@Override
	public void initialize(ConfigContainer parentConfig, InternalSocialNetworkCallback callback) {
		logger.debug("initialize");
		this.callback = callback;

		configRoot = parentConfig.createContainer("xmpp");
		user = configRoot.createOption(PROP_USER, ConfigOption.Type.TEXT, null);
		pass = configRoot.createOption(PROP_PASS, ConfigOption.Type.PASSWORD, null);
		server = configRoot.createOption(PROP_HOST, ConfigOption.Type.TEXT, null);
		port = configRoot.createOption(PROP_PORT, ConfigOption.Type.NUMBER, 5222);
		
		start();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.api.InternalSocialNetwork#getLogo()
	 */
	@Override
	public byte[] getLogo() {
		try {
			InputStream in = ClassLoader.getSystemResourceAsStream("images/icon-xmpp-16.png");
			byte[] logo = new byte[in.available()];
			in.read(logo);
			// TODO Auto-generated method stub
			return logo;
		} catch (Exception e) {
			logger.error("Failed reading resource: ",e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#getName()
	 */
	@Override
	public String getName() {
		return "XMPP";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#getURLScheme()
	 */
	@Override
	public String getURLScheme() {
		return "xmpp";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#getFeatures()
	 */
	@Override
	public Collection<Feature> getFeatures() {
		return Arrays.asList(Feature.APPLICATION_RPC);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#checkConnect()
	 */
	@Override
	public boolean checkConnect() {
		logger.debug("checkConnect");
		
		try {
			while (true) {
			switch (state) {
			case TRY_RECONNECT:
				// Ensure there is a configuration
				if (user.getStringValue()==null) {
					logger.warn("No XMPP configuration yet");
					return false;
				}
				evaluateConfig();
				
				if (serverList.isEmpty()) {
					if (buildServerList()) {
						state = ConnectionState.TRY_CONNECTION;
						continue;
					}
					state = ConnectionState.TRY_RECONNECT;
				} else {
					state = ConnectionState.TRY_CONNECTION;
					continue;						
				}
				break;
			case TRY_CONNECTION:
				if (serverList.isEmpty()) {

				} else {
					String server = serverList.get(0);
					if (makeConnection(server))
						continue;
				}
				break;
			case DISCONNECT_REQUESTED:
				logger.debug("Disconnect requested on "+conn);
				if (conn!=null)
					conn.disconnect();
				conn = null;
				resetServerList();
				Thread.sleep(1000);
				state = ConnectionState.TRY_RECONNECT;
				break;
			case CONNECTED:
				return true;
			case READY:
				return true;
			default:
				logger.warn("Don't know what to do in state: "+state);
			} // switch
		}
		} catch (InterruptedException e) {
			logger.debug("Interrupted - in state "+state);
		} catch (Exception e) {
			logger.error(e.toString(),e);
			state = ConnectionState.TRY_RECONNECT;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.ConfigChangeListener#configChanged(de.rpgframework.ConfigContainer, java.util.Collection)
	 */
	@Override
	public void configChanged(ConfigContainer source, Collection<ConfigOption<?>> options) {
		logger.info("Configuration changed - request disconnect and try again");
		state = ConnectionState.DISCONNECT_REQUESTED;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.social.OnlineService#getCharacterStorage()
	 */
	@Override
	public CharacterProvider getCharacterStorage() {
		return null;
	}

//	//------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgtool.InterComponentProtocol#getAvailableDevices()
//	 */
//	@Override
//	public Collection<RPGToolDevice> getAvailableDevices() {
//		if (gmRoster!=null)
//			return gmRoster.getAvailableDevices();
//		return new ArrayList<RPGToolDevice>();
//	}
//
//	//------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgtool.InterComponentProtocol#configure(java.util.Properties)
//	 */
//	@Override
//	public void configure(Properties pro) {
//		logger.debug("configure");
//		if (config==null)
//			config = new XMPPConfig(pro);
//		else
//			config.readFromProperties(pro);
//		
//		if (myState==ProtocolState.UNCONFIGURED)
//			myState = ProtocolState.CONFIGURED;
//	}
//
//	//------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgtool.InterComponentProtocol#getConfig()
//	 */
//	@Override
//	public List<ConfigValue> getConfig() {
//		logger.debug("getConfig() = "+config);
//		if (config==null)
//			System.exit(0);
//		List<ConfigValue> ret = new ArrayList<ConfigValue>();
//		ret.add(new ConfigValue(ID_IDENTITY, config.getIdentity()));
//		ret.add(new ConfigValue(ID_PASSWORD, config.getPassword()));
//		if (config.getServerHost()!=null)
//			ret.add(new ConfigValue(ID_SERVHOST, config.getServerHost()));
//		if (config.getServerPort()!=5222)
//			ret.add(new ConfigValue(ID_SERVPORT, config.getServerPort()));
//		
//		return ret;
//	}
//
//	//------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgtool.InterComponentProtocol#setConfig(java.util.List)
//	 */
//	@Override
//	public void setConfig(List<ConfigValue> newConfig) {
//		logger.info("TODO: Config changed: "+newConfig);
//		
//		for (ConfigValue newValue : newConfig) {
//			if (newValue.id.equals(ID_IDENTITY))
//				config.setIdentity((String) newValue.value);
//			else if (newValue.id.equals(ID_PASSWORD))
//				config.setPassword((String) newValue.value);
//			else if (newValue.id.equals(ID_SERVHOST))
//				config.setServerHost((String) newValue.value);
//			else if (newValue.id.equals(ID_SERVPORT))
//				config.setServerPort((Integer) newValue.value);
//		}
//		
//		stop();
//	}

//	//------------------------------------------------------------------
//	/**
//	 * @param callback the callback to set
//	 */
//	public void setCallback(XMPPClientCallback callback) {
//		this.callback = callback;
//	}

}
