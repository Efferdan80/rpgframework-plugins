/**
 * 
 */
package org.prelle.rpgframework.xmpp.ext;

/**
 * @author prelle
 *
 */
public enum MessageType {

	DISCOVERY,
	SEND_TEXT_MESSAGE,
	SHOW_HANDOUT
	
}
