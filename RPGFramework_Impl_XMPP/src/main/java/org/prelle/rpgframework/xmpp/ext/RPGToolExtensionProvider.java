/**
 * 
 */
package org.prelle.rpgframework.xmpp.ext;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * @author prelle
 *
 */
public class RPGToolExtensionProvider implements PacketExtensionProvider {

	private final static Logger logger = Logger.getLogger("rpgtool.xmpp.parser");

	public final static String NAMESPACE = "http://prelle.org/protocols/rpgtool";

	//------------------------------------------------------------------
	/**
	 */
	public RPGToolExtensionProvider() {
		// TODO Auto-generated constructor stub
	}

	//------------------------------------------------------------------
	/**
	 * @see org.jivesoftware.smack.provider.PacketExtensionProvider#parseExtension(org.xmlpull.v1.XmlPullParser)
	 */
	@Override
	public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
		logger.trace("parseExtension called on "+parser.getName());

		String expected = null;
		try {
			int eventType = parser.getEventType();
			//            logger.debug("parse first "+parser.getName()+" / "+XmlPullParser.TYPES[eventType]);

			if (eventType != XmlPullParser.START_TAG) {
				throw new Exception("Expected start tag");
			}

			boolean done = false;
			expected = null;
			parser.nextTag();
			while (!done) {
				eventType = parser.getEventType();
				//	            logger.debug("parse next "+parser.getName()+" / "+XmlPullParser.TYPES[eventType]);

				if (eventType == XmlPullParser.START_TAG) {
//					if ("discover".equals(parser.getName())) {
//						//			        	logger.debug("Discovery recognized");
//						return parseAsDiscoveryPacket(parser);
//					} else if ("showtext".equals(parser.getName())) {
//						logger.debug("showtext recognized");
//						return parseAsShowTextMessage(parser);
//					} else if ("handout".equals(parser.getName())) {
//						logger.debug("handout recognized");
//						return parseAsShowHandout(parser);
//					} else  {
						expected = parser.getName();
						logger.warn("Unsupported command: "+parser.getName());
						readUntilEndTag(parser, parser.getName());
						return null;
//					}
				}
				else {
					parser.nextTag();
				}
			}
		} catch (Exception e) {
			logger.error(e.toString(),e);
		}

		throw new Exception("Unsupported command: "+expected);
	}

	//------------------------------------------------------------------
	private void readUntilEndTag(XmlPullParser parser, String expected) throws XmlPullParserException, IOException {
		parser.nextTag();
		while (true) {
			int eventType = parser.getEventType();
			logger.trace("parse next "+parser.getName()+" / "+XmlPullParser.TYPES[eventType]);

			if (eventType == XmlPullParser.END_TAG) {
				if (expected.equals(parser.getName())) {
					return;
				}
				else {
					parser.nextTag();
				}
			}
			else {
				parser.nextTag();
			}
		}

	}

//	//------------------------------------------------------------------
//	private DiscoveryPacket parseAsDiscoveryPacket(XmlPullParser parser) throws Exception {
//		logger.debug("parseAsDiscoveryPacket("+parser.getName()+")");
//		String endTag = parser.getName();
//
//		String role_s = parser.getAttributeValue("", "role");        
//		if (role_s == null) {
//			logger.warn("Parse error: discover-Element without role-attribute");
//			throw new Exception("Missing role");
//		}
//		try {
//			Role role = Role.valueOf(role_s); 
//
//			readUntilEndTag(parser, endTag);
//
//			logger.debug("Discover packet read");
//			return new DiscoveryPacket(role);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	//------------------------------------------------------------------
//	private ShowTextMessage parseAsShowTextMessage(XmlPullParser parser) throws Exception {
//		logger.debug("parseAsShowText("+parser.getName()+")");
//		String mess;
//		try {
//			String endTag = parser.getName();
//
//			String seconds_s = parser.getAttributeValue("", "sec");  
//			int seconds = 0;
//			try {
//				seconds = Integer.parseInt(seconds_s);
//			} catch (NumberFormatException e) {
//				logger.warn("showText element contains a sec-Attribute which isn't a number");
//			}
//
//			mess = parser.nextText();
//			logger.debug("Message is: "+mess+"  // Now parse until "+endTag);
//
////			readUntilEndTag(parser, endTag);
//			logger.debug("Read: "+mess);
//			return new ShowTextMessage(mess, seconds);
//		} catch (Exception e) {
//			logger.error(e.toString(),e);
//		}
//		logger.debug("Failed parsing");
//		return null;
//	}
//
//	//------------------------------------------------------------------
//	private ShowHandoutPacket parseAsShowHandout(XmlPullParser parser) throws Exception {
//		logger.debug("parseAsShowHandout("+parser.getName()+")");
//		try {
//			String uri = parser.getAttributeValue("", "uri");  
//			logger.debug("Read: "+uri);
//			return new ShowHandoutPacket(uri);
//		} catch (Exception e) {
//			logger.error(e.toString(),e);
//		}
//		logger.debug("Failed parsing");
//		return null;
//	}

}
