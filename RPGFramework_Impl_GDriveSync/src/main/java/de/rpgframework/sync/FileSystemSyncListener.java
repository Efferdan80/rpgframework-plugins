/**
 * 
 */
package de.rpgframework.sync;

import java.nio.file.Path;

/**
 * @author prelle
 *
 */
public interface FileSystemSyncListener {

	public void localDirectoryCreated(Path path);

}
