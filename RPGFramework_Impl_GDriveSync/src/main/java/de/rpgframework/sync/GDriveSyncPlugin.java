/**
 * 
 */
package de.rpgframework.sync;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.prelle.gdrivefs.GoogleDriveFSConstants;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkPlugin;
import de.rpgframework.sync.FileSystemSync.SyncMode;

/**
 * @author prelle
 *
 */
public class GDriveSyncPlugin implements RPGFrameworkPlugin {

	private final static Logger logger = Logger.getLogger("rpgframework.sync");
	
	private FileSystem localFS;
	private FileSystem remoteFS;
	private Path localPath;
	private Path remotePath;

	//-------------------------------------------------------------------
	public GDriveSyncPlugin() {
		logger.debug("<init>");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RPGFrameworkPlugin#initialize(de.rpgframework.RPGFramework)
	 */
	@Override
	public void initialize(RPGFramework framework) {
		logger.warn("initialize");
		localFS = FileSystems.getDefault();
		ConfigOption<String> cfgDataDir = (ConfigOption<String>) framework.getConfiguration().getOption(RPGFramework.PROP_DATADIR);
		localPath = Paths.get((String)cfgDataDir.getValue());
		logger.info("Local sync directory : "+localPath);

		
		String uri = String.format("google:/");
		Map<String, Object> env = new HashMap<>();
		env.put(GoogleDriveFSConstants.SCOPE, GoogleDriveFSConstants.SCOPE_FILE);
		env.put(GoogleDriveFSConstants.APPLICATION_NAME, "RPGFramework");
		env.put(GoogleDriveFSConstants.CLIENT_SECRETS, ClassLoader.getSystemResourceAsStream("client_secrets.json"));
		try {
			remoteFS = FileSystems.newFileSystem(URI.create(uri), env);
			remotePath = remoteFS.getPath("RPGFramework");
			logger.info("Remote sync directory: "+remotePath);
		} catch (IOException e) {
			logger.fatal("Failed connecting to Google Drive. Synchronisation disabled",e);
		}

		FileSystemSync sync = new FileSystemSync("test", localPath, SyncMode.UPLOAD_ONLY, new FileSystemSyncListener() {

			@Override
			public void localDirectoryCreated(Path path) {
				logger.info("local directory created: "+path);
			}});
		logger.info("Adding remote directory and start syncing");
		sync.addRemoteRoot("gdrive",remotePath);
	}

}
