/**
 * 
 */
package de.rpgframework.upnp;

import org.fourthline.cling.binding.annotations.*;

/**
 * @author prelle
 *
 */
@UpnpService(
        serviceId = @UpnpServiceId(namespace=Constants.NAMESPACE, value = Constants.ID_GAMEMASTER),
        serviceType = @UpnpServiceType(value = Constants.ID_GAMEMASTER, version = 1)
)
public class GamemasterService {

	//------------------------------------------------------------------
	/**
	 */
	public GamemasterService() {
		// TODO Auto-generated constructor stub
	}

}
