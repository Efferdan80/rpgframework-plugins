/**
 * 
 */
package de.rpgframework.upnp;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.message.header.STAllHeader;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.DeviceIdentity;
import org.fourthline.cling.model.meta.Icon;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.meta.ManufacturerDetails;
import org.fourthline.cling.model.meta.ModelDetails;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.model.meta.Service;
import org.fourthline.cling.model.types.DeviceType;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;

import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkPlugin;
import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.devices.DeviceFunction;
import de.rpgframework.devices.RPGToolDevice;
import de.rpgframework.upnp.device.UPnPDevice;
import de.rpgframework.upnp.device.UPnPMediaRenderer;
import de.rpgframework.upnp.service.AVTransportService;
import de.rpgframework.upnp.service.ConnectionManagerService;
import de.rpgframework.upnp.service.RenderingControlService;

/**
 * @author prelle
 *
 */
public class UPnPClient implements Runnable, RegistryListener, RPGFrameworkPlugin {

	private final static Logger logger = Logger.getLogger("rpgframework.upnp");
	
	private UpnpService upnpService;
//	private List<RPGToolComponentListener> componentListener;
	
	private String mediaServerURI;
	private UPnPMediaRenderer mediaRenderer;
	private Map<RemoteDevice, RPGToolDevice> deviceMap;
	
	private Service contentDir;

	private Thread thread;
	
	//------------------------------------------------------------------
	public UPnPClient() {
    	java.util.logging.Logger.getLogger("org.fourthline.cling").setLevel(Level.SEVERE);
    	java.util.logging.Logger.getLogger("org.fourthline.cling.protocol.RetrieveRemoteDescriptors").setLevel(Level.SEVERE);
    	deviceMap = new HashMap<RemoteDevice, RPGToolDevice>();
    	
//    	componentListener = new ArrayList<RPGToolComponentListener>();
//    	upnpService = new UpnpServiceImpl(this);
//
//        // Send a search message to all devices and services, they should respond soon
//        upnpService.getControlPoint().search(new STAllHeader());
//		
//        try {
//			LocalDevice device = createGamemasterDevice();
//			upnpService.getRegistry().addDevice(
//                    device
//            );
//		} catch (Exception e) {
//			logger.error(e.toString(),e);
//			upnpService.shutdown();
//			System.exit(0);
//		}
    	
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RPGFrameworkPlugin#initialize(de.rpgframework.RPGFramework)
	 */
	@Override
	public void initialize(RPGFramework framework) {
		logger.info("Initializing");
    	thread = new Thread(this, "UPnPClient");
    	thread.setDaemon(true);
    	thread.start();
	}

	//------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private LocalDevice createGamemasterDevice()
            throws ValidationException, LocalServiceBindingException, IOException {

        DeviceIdentity identity =
                new DeviceIdentity(
                        UDN.uniqueSystemIdentifier("BlaFaselDuddelDu")
                );

        DeviceType type =
                new UDADeviceType("rpgtool-gamemaster", 1);

        DeviceDetails details =
                new DeviceDetails(
                        "RPGTool Spielleiter",
                        new ManufacturerDetails("Stefan Prelle","http://www.prelle.org/stefan"),
                        new ModelDetails(
                                "RPGTool-GM",
                                "Software zur Spielleiterunterstützung",
                                "0.2"
                        )
                );

        Icon icon =null;
//                new Icon(
//                        "image/png", 48, 48, 8,
//                        getClass().getResource("icon.png")
//                );

        LocalService<GamemasterService> gmService =
                new AnnotationLocalServiceBinder().read(GamemasterService.class);

        gmService.setManager(
                new DefaultServiceManager<GamemasterService>(gmService, GamemasterService.class){
                	@Override
                	public GamemasterService getImplementation() {
                		logger.debug("getImplementation() called");
                		return new GamemasterService();
                	}                	
                }
        );

        return new LocalDevice(identity, type, details, icon, gmService);

        /* Several services can be bound to the same device:
        return new LocalDevice(
                identity, type, details, icon,
                new LocalService[] {switchPowerService, myOtherService}
        );
        */
        
    }

//	//------------------------------------------------------------------
//	private LocalDevice createSessionScreenDevice(final SessionScreen realScreen)
//            throws ValidationException, LocalServiceBindingException, IOException {
//
//        DeviceIdentity identity =
//                new DeviceIdentity(
//                        UDN.uniqueSystemIdentifier("BlaFaselDuddelDu2")
//                );
//
//        DeviceType type =
//                new UDADeviceType("rpgtool-sessionscreen", 1);
//
//        DeviceDetails details =
//                new DeviceDetails(
//                        "RPGTool Bildschirm",
//                        new ManufacturerDetails("Stefan Prelle","http://www.prelle.org/stefan"),
//                        new ModelDetails(
//                                "RPGTool",
//                                "Tool zur Ansteuerung des Spielbildschirms",
//                                "0.1"
//                        )
//                );
//
//        Icon icon =null;
////                new Icon(
////                        "image/png", 48, 48, 8,
////                        getClass().getResource("icon.png")
////                );
//
//        LocalService<GamemasterService> ssService =
//                new AnnotationLocalServiceBinder().read(UPnPSessionScreenServer.class);
//
//        ssService.setManager(
//                new DefaultServiceManager(ssService, UPnPSessionScreenServer.class){
//                	@Override
//                	public UPnPSessionScreenServer getImplementation() {
//                		logger.debug("getImplementation() called");
//                		return new UPnPSessionScreenServer(realScreen);
//                	}                	
//                }
//        );
////        ssService.setManager(
////                new DefaultServiceManager(ssService, UPnPSessionScreenServer.class)
////        );
//
//        return new LocalDevice(identity, type, details, icon, ssService);
//    }

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#afterShutdown()
	 */
	@Override
	public void afterShutdown() {
		// TODO Auto-generated method stub
		
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#beforeShutdown(org.fourthline.cling.registry.Registry)
	 */
	@Override
	public void beforeShutdown(Registry arg0) {
		// TODO Auto-generated method stub
		
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#localDeviceAdded(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.LocalDevice)
	 */
	@Override
	public void localDeviceAdded(Registry arg0, LocalDevice device) {
		// TODO Auto-generated method stub
		logger.debug("localDeviceAdded: "+device.getDisplayString());
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#localDeviceRemoved(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.LocalDevice)
	 */
	@Override
	public void localDeviceRemoved(Registry arg0, LocalDevice arg1) {
		// TODO Auto-generated method stub
		
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#remoteDeviceAdded(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.RemoteDevice)
	 */
	@Override
	public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
		logger.info("---------------------------------------------------------------------");
		logger.info("Remote device added: "+device.getType()+" / "+device.getDisplayString()+" // "+device.getDetails().getFriendlyName());
		// For testing: Ignore all Microsoft MediaPlayer
//		if (device.getDisplayString().contains("Microsoft"))
//			return;
		
		/*
		 * Create a matching device following the announced device type
		 */
		UPnPDevice data = null;
		DeviceType type = device.getType();
		switch (type.getNamespace()) {
		case "schemas-upnp-org":
			switch (type.getType()) {
			case "MediaRenderer":
				logger.info("Found a renderer: "+device);
				data = new UPnPMediaRenderer(upnpService, device);
				break;
			default:
				logger.warn("Unsupported device: "+type);
				return;
			}
			break;
		case "dial-multiscreen-org":
			logger.warn("Ignore DIAL device "+device);
			return;
		default:
			logger.warn("Unsupported device namespace: "+type.getNamespace());
			return;
		}
		

		/*
		 * Now add services to device
		 */
		for (RemoteService serv : device.getServices()) {
			logger.warn("  Provides service "+serv.getServiceId().getId()+" / "+serv.getServiceId().getNamespace());

			// Standard UPnP
			switch (serv.getServiceId().getNamespace()) {
			case "upnp-org":
				switch (serv.getServiceId().getId()) {
				case "ConnectionManager":
					data.addService(new ConnectionManagerService(device, upnpService, serv));
					break;
				case "RenderingControl":
					data.addService(new RenderingControlService(device, upnpService, serv));
					break;
				case "AVTransport":
					logger.info("  Add service AVTransportService");
					data.addService(new AVTransportService(device, upnpService, serv));
					break;
				default:
					logger.warn("Unsupported service: "+serv.getServiceId().getId());
				}
				break;
			case "microsoft.com":
				// Ignore microsoft proprietary
				break;
			default:
				logger.warn("Unsupported service namespace: "+serv.getServiceId().getNamespace());
			}
			
//			if (serv.getServiceId().getId().equals("MessageBoxService")) {
//				logger.info("Found a Samsung screen");
//				SamsungUPnPSessionScreenClient sessScreen = new SamsungUPnPSessionScreenClient(upnpService, device);
//			}
		}
		data.serviceDetectionFinished();
		
//		DeviceType type = device.getType();
//		logger.debug("  Namespace "+type.getNamespace()+"  type="+type.getType());
//		if (type.getNamespace().equals("schemas-upnp-org") && type.getType().equals("MediaServer")) 
//			remoteMediaServerFound(device);
//		if (type.getNamespace().equals("schemas-upnp-org") && type.getType().equals("MediaRenderer")) 
//			data = remoteMediaRendererFound(device, data);
//		if (type.getNamespace().equals("dial-multiscreen-org")) 
//			dialDeviceFound(device, data);
		deviceMap.put(device, data);
//		logger.debug("  DisplayString = "+type.getDisplayString());
//		logger.debug("  Namespace = "+type.getNamespace());
//		logger.debug("  Type = "+type.getType());
//		logger.debug("Device Functions = "+data.getSupportedFunctions());

		logger.debug("  Added "+data.getName()+" is of type "+data.getClass());
		EventBus.fireEvent(this, EventType.DEVICE_APPEARED, data);
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#remoteDeviceDiscoveryFailed(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.RemoteDevice, java.lang.Exception)
	 */
	@Override
	public void remoteDeviceDiscoveryFailed(Registry arg0, RemoteDevice arg1,
			Exception arg2) {
		// TODO Auto-generated method stub
		
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#remoteDeviceDiscoveryStarted(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.RemoteDevice)
	 */
	@Override
	public void remoteDeviceDiscoveryStarted(Registry arg0, RemoteDevice arg1) {
		// TODO Auto-generated method stub
		
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#remoteDeviceRemoved(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.RemoteDevice)
	 */
	@Override
	public void remoteDeviceRemoved(Registry arg0, RemoteDevice device) {
		logger.debug("Remote device removed: "+device.getDisplayString());
		if (!deviceMap.containsKey(device)) {
			logger.debug("The disappearing remote device was unknown");
			return;
		}
		
		RPGToolDevice wrappedDev = deviceMap.get(device);
		deviceMap.remove(device);
		
		if (wrappedDev!=null) {
			EventBus.fireEvent(this, EventType.DEVICE_DISAPPEARED, wrappedDev);
		}
	}

	//------------------------------------------------------------------
	/**
	 * @see org.fourthline.cling.registry.RegistryListener#remoteDeviceUpdated(org.fourthline.cling.registry.Registry, org.fourthline.cling.model.meta.RemoteDevice)
	 */
	@Override
	public void remoteDeviceUpdated(Registry arg0, RemoteDevice device) {
	}

	//------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.InterComponentProtocol#startAs(org.prelle.rpgtool.RPGToolDevice)
	 */
//	@Override
	public void startAs(RPGToolDevice realDevice) {
		logger.debug("start as "+Arrays.asList(realDevice.getSupportedFunctions()));
		
//		Thread serverThread = new Thread( this, "UPnP");
//		serverThread.setDaemon(false);
//		serverThread.start();
    	upnpService = new UpnpServiceImpl(this);

        // Send a search message to all devices and services, they should respond soon
        upnpService.getControlPoint().search(new STAllHeader());
		
//        try {
//        	switch (realDevice.getDeviceType()) {
//        	case GAMEMASTER:
//    			upnpService.getRegistry().addDevice(createGamemasterDevice());
//    			break;
//        	case SESSION_SCREEN:
//    			upnpService.getRegistry().addDevice(createSessionScreenDevice((SessionScreen)realDevice));
//				break;
//        	case PLAYER_UI:
//        	default:
//        		logger.warn("Don't know how to start a "+realDevice.getDeviceType());
//        	}
//		} catch (Exception e) {
//			logger.error(e.toString(),e);
//			upnpService.shutdown();
//			System.exit(0);
//		}
	}

	//------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		logger.debug("Starting "+Thread.currentThread().getName());
		RPGToolDevice gamemasterDevice = new RPGToolDevice() {
			
			@Override
			public int compareTo(RPGToolDevice arg0) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return "Gamemaster";
			}
			
			@Override
			public byte[] getIcon() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getFunction(DeviceFunction func) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Collection<DeviceFunction> getSupportedFunctions() {
				// TODO Auto-generated method stub
				return new ArrayList<DeviceFunction>();
			}

			@Override
			public void activate() {
				// TODO Auto-generated method stub
				logger.warn("TODO: activate");
			}
			@Override
			public void deactivate() {
				// TODO Auto-generated method stub
				logger.warn("TODO: deactivate");
			}
			
		};
		startAs(gamemasterDevice);
	}

//	//------------------------------------------------------------------
//	/**
//	 * @return Content ID
//	 */
//	private String getPath(Service contentDir, String containerID, StringTokenizer tok) {
//		final String search = tok.nextToken();
//		
//		MyBrowse browse = new MyBrowse(contentDir, containerID, BrowseFlag.DIRECT_CHILDREN);
//
//		synchronized (browse) {
//			upnpService.getControlPoint().execute(browse);
//			try {
//				browse.wait(5000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
////		logger.debug("Found "+browse.getContainer());
//		for (Container cont : browse.getContainer()) {
//			new RemoteFileHandle(cont.getId(), cont.getTitle(), false);
//			new ContainerFileHandleImpl(cont);
//			if (cont.getTitle().equals(search)) {
//				if (tok.hasMoreTokens()) {
//					logger.debug("Found "+search+" recurse to "+cont.getId());
//					return getPath(contentDir, cont.getId(), tok);
//				} else {
//					logger.debug("Found "+search+" - finally");
//					return cont.getId();
//				}
//			}
//		}
//		for (Item cont : browse.getItems()) {
//			if (cont.getTitle().equals(search)) {
//				new RemoteFileHandle(cont.getId(), cont.getTitle(), false);
//				new ItemFileHandle(cont);
////				logger.debug("Title      = "+cont.getTitle());
////				logger.debug("Creator    = "+cont.getCreator());
////				logger.debug("Clazz      = "+cont.getClazz());
////				logger.debug("WriteStatus= "+cont.getWriteStatus());
////				logger.debug("Properties:");
////				for (Property prop : cont.getProperties()) {
////					logger.debug("item prop: "+prop.getDescriptorName()+" = "+prop.getValue());
////				}
////				logger.debug("Resources:");
////				for (Res res : cont.getResources()) {
////					logger.debug("item Res Duration  : "+res.getDuration());
////					logger.debug("item Res Resolution: "+res.getResolution());
////					logger.debug("item Res Value     : "+res.getValue());
////					logger.debug("item Res ProtInfo  : "+res.getProtocolInfo());
////					logger.debug("item Res Size      : "+res.getSize());
////				}
////				logger.debug("DescMeta:");
////				for (DescMeta meta: cont.getDescMetadata()) {
////					logger.debug("item meta: "+meta.getId()+" = "+meta.getMetadata());
////				}
//				if (tok.hasMoreTokens()) {
//					logger.debug("Found "+search+" - more requested but cannot recurse to into item");
//				} else {
//					logger.debug("Found "+search+" / "+cont.getId()+" - finally");
//				}
//				return cont.getFirstResource().getValue();
//			}
//		}
//		
//		return null;
//	}

	//------------------------------------------------------------------
	private void dialDeviceFound(RemoteDevice device, UPnPDevice dialDev) {
		logger.info("DIAL device found: "+dialDev.getName());

		logger.debug("  Parent device = "+device.getParentDevice());
		logger.debug("  Desc URL "+device.getIdentity().getDescriptorURL());
		
		try {
			HttpURLConnection con = (HttpURLConnection)device.getIdentity().getDescriptorURL().openConnection();
			con.connect();
			int code = con.getResponseCode();
			logger.debug("  code = "+code);
			logger.debug("  App-URL "+con.getHeaderField("Application-URL"));
			
			String baseURL = con.getHeaderField("Application-URL");

			String[] APPNAMES = {
					"YouTube","Netflix","iPlayer","News","Sport","flingo","samba","Pandora","Radio","Hulu","KontrolTV","tv.primeguide","primeguide",
					"Olympus","com.dailymotion","Dailymotion","SnagFilms","Twonkey TV","Turner-TNT-Leverage","Turner-TBS-BBT","Turner-NBA_GameTime",
					"Turner-TNT-FallingSkies","WiDi","cloudmedia","freeott","popcornhour","Turner-TBS-TeamCoco","RedBoxInstant","YuppTV.Remote",
					"D-Smart","D-SmartBlu","Gladiator","com.lge","JustMirroring","ConnectedRedButton","sling.player.googletv","Famium","tv.boxee.cloudee",
					"freesat","freetime","com.humax","YahooScreen","Grooveshark","Vimeo","ObsidianX","cloudtv","porn","pornz","Plex","Game",
					"comcast","com.comcast","RichMediaPlayer","RealPlayer Cloud","AmazonInstantVideo","com.nowtv.NowTV","App Search","ScreenCloud",
					"HbbTV","PlayTo","TVPlayer"};
			for (String app : APPNAMES) {
				URL url = new URL(baseURL+app);
				con = (HttpURLConnection)url.openConnection();
				con.connect();
				try {
					code = con.getResponseCode();
					logger.debug("DIAL  "+dialDev.getName()+" \t"+app+" \t= "+code);
				} catch (Exception e) {
					logger.debug("DIAL  "+dialDev.getName()+" \t"+app+" \t= "+e.getMessage());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		logger.fatal("Stop here");
//		System.exit(0);
	}

}
