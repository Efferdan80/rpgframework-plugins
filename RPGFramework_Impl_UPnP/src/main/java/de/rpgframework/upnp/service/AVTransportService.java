/**
 * 
 */
package de.rpgframework.upnp.service;

import java.util.ArrayList;
import java.util.List;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.support.avtransport.callback.Play;
import org.fourthline.cling.support.avtransport.callback.SetAVTransportURI;
import org.fourthline.cling.support.avtransport.callback.Stop;

/**
 * @author prelle
 *
 */
public class AVTransportService extends UPnPService {

	//-------------------------------------------------------------------
	public AVTransportService(RemoteDevice device, UpnpService upnpService, RemoteService service) {
		super(device, upnpService, service);
	}

	//-------------------------------------------------------------------
	public boolean setAVTransportURI(String uri) {
		final List<Boolean> result = new ArrayList<>();
		Boolean waitLock = Boolean.valueOf(true);

		SetAVTransportURI set = new SetAVTransportURI(realService, uri) {
			@SuppressWarnings("rawtypes")
			public void failure(ActionInvocation arg0, UpnpResponse response, String reason) {
				logger.error("failure in SetAVTransportURI: "+response +" reason="+reason);
				synchronized (waitLock) {
					result.add(false);
					waitLock.notify();
				}
			}
			@SuppressWarnings("rawtypes")
			public void success(ActionInvocation action) {
				logger.debug("Successfully set AVTransportURI to "+uri);
				synchronized (waitLock) {
					result.add(true);
					waitLock.notify();
				}
			}
		};
		logger.debug("Calling setAVTransportURI("+uri+")");
		synchronized (waitLock) {
			upnpService.getControlPoint().execute(set);

			try {
				waitLock.wait(1000);
			} catch (InterruptedException e) {
				logger.warn("Interrupted");
			}
		}

		if (result.isEmpty()) return false;
		return result.get(0);

	}

	//------------------------------------------------------------------
	public void play() {
		Play play = new Play(realService) {
			@SuppressWarnings("rawtypes")
			public void failure(ActionInvocation arg0, UpnpResponse resp, String arg2) {
				logger.warn("failure in play");
			}
		};
		logger.debug("Calling play");
		upnpService.getControlPoint().execute(play);
	}

	//------------------------------------------------------------------
	public void stop() {
		Stop stop = new Stop(realService) {
			@SuppressWarnings("rawtypes")
			public void failure(ActionInvocation arg0, UpnpResponse resp, String arg2) {
				logger.warn("failure in stop");
			}
		};
		logger.debug("Calling stop");
		upnpService.getControlPoint().execute(stop);
	}
}
