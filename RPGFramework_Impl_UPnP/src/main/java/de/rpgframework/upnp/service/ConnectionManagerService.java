/**
 * 
 */
package de.rpgframework.upnp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.message.UpnpResponse;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.model.meta.RemoteService;
import org.fourthline.cling.support.connectionmanager.callback.GetProtocolInfo;
import org.fourthline.cling.support.model.ProtocolInfos;

/**
 * @author prelle
 *
 */
public class ConnectionManagerService extends UPnPService {
	private List<String> supportedProtocols;
	
	//-------------------------------------------------------------------
	public ConnectionManagerService(RemoteDevice device, UpnpService upnpService, RemoteService service) {
		super(device, upnpService, service);
		supportedProtocols = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public List<String> getSupportedProtocols() {
		if (!supportedProtocols.isEmpty())
			return supportedProtocols;
		
		GetProtocolInfo get = new GetProtocolInfo(realService, upnpService.getControlPoint()) {
			
			@SuppressWarnings("rawtypes")
			public void failure(ActionInvocation action, UpnpResponse resp, String reason) {
				logger.warn("  GetProtocolInfo failed: "+reason);
			}
			
			@SuppressWarnings("rawtypes")
			@Override
			public void received(ActionInvocation arg0, ProtocolInfos sink,
					ProtocolInfos source) {
				logger.debug("  GetProtocolInfo succeded");
				StringTokenizer tok = new StringTokenizer(sink.toString(), ",");
				while (tok.hasMoreTokens()) {
					String support = tok.nextToken();
					supportedProtocols.add(support);
				}
				Collections.sort(supportedProtocols);
				// Notify
				synchronized (supportedProtocols) {
					supportedProtocols.notify();
				}
			}
		};
		
		synchronized (supportedProtocols) {
			logger.debug("Call getSupp");
			upnpService.getControlPoint().execute(get);
			try {
				supportedProtocols.wait(3000);
			} catch (InterruptedException e) {
				logger.warn("Interrupted while waiting for UPnP");
			}
		}

		return supportedProtocols;
	}

}
